###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 03b - Animal Farm 1
#
# @file Makefile
# @version 1.0
#
# @author Marie Wong <marie4@hawaii.edu>
# @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
# @date   26_JAN_2021
###############################################################################

CC		 = gcc
CFLAGS = -g

TARGET = animalfarm

all: animalfarm

animals.o: animals.c animals.h
	$(CC) $(CFLAGS) -c animals.c

cat.o: cat.c cat.h
	$(CC) $(CFLAGS) -c cat.c

main.o: main.c
	$(CC) $(CFLAGS) -c main.c

animalfarm: animals.o cat.o main.o 
	$(CC) $(CFLAGS) -o $(TARGET) animals.o cat.o main.o

clean:
	rm -f *.o $(TARGET)
