///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file animals.c
/// @version 1.0
///
/// Helper functions that apply to animals great and small
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date 26_JAN_2021 
///////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>

#include "animals.h"

/// Decode the enum Color into strings for printf()
char* colorName (enum Color color) {

   // Map the enum Color to a string
   switch(color){  
      case BLACK: return "black";
      case WHITE: return "white";
      case RED:   return "red";
      case BLUE:  return "blue";
      case GREEN: return "green";
      case PINK:  return "pink";
   }
   
   return NULL; // We should never get here
};

