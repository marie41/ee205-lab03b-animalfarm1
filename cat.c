///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 03b - Animal Farm 1
///
/// @file cat.c
/// @version 1.0
///
/// Implements a simple database that manages cats
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 03b - AnimalFarm1 - EE 205 - Spr 2021
/// @date   10_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "cat.h"

// Allocates cat database
struct Cat catDB[MAX_SPECIES];

//returns string of breed name
char* catBreedName (enum CatBreeds abreed) {

   // Map the enum Color to a string
   switch(abreed){  
      case MAIN_COON:   return "Main Coon";
      case MANX:        return "Manx";
      case SHORTHAIR:   return "Shorthair";
      case PERSIAN:     return "Persian";
      case SPHYNX:      return "Sphynx";
      default:          return "UNKNOWN";
   }

   return NULL;
}


/// Add Alice to the Cat catabase at position i.
/// 
/// @param int i Where in the catDB array that Alice the cat will go
///
/// @note This terrible style... we'd never hardcode this data, but it gets us started.
void addAliceTheCat(int i) {

   strcpy(catDB[i].name, "Alice");
   catDB[i].gender = FEMALE;
   catDB[i].breed = MAIN_COON;
   catDB[i].isFixed = true;
   catDB[i].weight = 12.34;
   catDB[i].collar1_color = BLACK;
   catDB[i].collar2_color = RED;
   catDB[i].license = 12345;
}



/// Print a cat from the database
/// 
/// @param int i Which cat in the database that should be printed
///
void printCat(int i) {
   
   printf ("Cat name = [%s]\n"            , catDB[i].name );
   printf ("    gender = [%s]\n"          , (catDB[i].gender == FEMALE) ? "Female" : "Male" );
   printf ("    breed = [%s]\n"           , catBreedName( catDB[i].breed ));
   printf ("    isFixed = [%s]\n"         , (catDB[i].isFixed) ? "Yes" : "No" );
   printf ("    weight = [%.2f]\n"        , catDB[i].weight );
   printf ("    collar color 1 = [%s]\n"  , colorName( catDB[i].collar1_color ));
   printf ("    collar color 2 = [%s]\n"  , colorName( catDB[i].collar2_color ));
   printf ("    license = [%ld]\n"        , catDB[i].license );
}

